﻿using System;

namespace hello
{
    class Program
    {
        //string s1 = "test";

        static void Main(string[] args)
        {
            Console.WriteLine("Hello Cruel World!!");
        }

       /* This section will cause static analysis failure, uncomment to test
        static void testError(string s1)
        {
            if (s1 == "")
            {
                Console.WriteLine("s1 equals empty string.");
            }
        }
        */
    }
}
